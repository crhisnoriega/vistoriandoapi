module.exports = function(Sequelize, sequelize) {
  class EnvironmentItem extends Sequelize.Model {}
  EnvironmentItem.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      name: Sequelize.STRING,
      description: Sequelize.STRING,
      type: Sequelize.STRING,
      subEnvironmentId: Sequelize.STRING,
      selection: {
        type: Sequelize.ENUM,
        values: ["unique", "multiple", "custom"]
      },

      options: {
        type: Sequelize.TEXT("long"),
        get: function() {
          try {
            var json = JSON.parse(this.getDataValue("options"));
            console.log(json);
            return json;
          } catch (e) {}
        },
        set: function(val) {
          try {
            return this.setDataValue("options", JSON.stringify(val));
          } catch (e) {}
        }
      },
      environmentId: Sequelize.STRING
    },
    { sequelize, modelName: "environmentitem" }
  );

  return EnvironmentItem;
};
