module.exports = function(Sequelize, sequelize) {
  class Establishment extends Sequelize.Model {}
  Establishment.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: Sequelize.STRING,
      shortName: Sequelize.STRING,
      description: Sequelize.STRING,
      latitude: Sequelize.DOUBLE,
      longitude: Sequelize.DOUBLE
     },
    { sequelize, modelName: "establishment" }
  );

  return Establishment;
};
