const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  "mysql://vistoriando:3&9Zy2pn@db.vistoriando.com/vistoriando"
);

const bcrypt = require("bcrypt");

const Surveyor = require("../models/Surveyor")(Sequelize, sequelize);
const Agency = require("../models/Agency")(Sequelize, sequelize);
const Property = require("../models/Property")(Sequelize, sequelize);
const Customer = require("../models/Customer")(Sequelize, sequelize);

const Surver = require("../models/Surver")(Sequelize, sequelize);
const Model = require("../models/Model")(Sequelize, sequelize);
const Service = require("../models/Service")(Sequelize, sequelize);

const Key = require("../models/Key")(Sequelize, sequelize);
const Medidor = require("../models/Medidor")(Sequelize, sequelize);
const Expediente = require("../models/Expediente")(Sequelize, sequelize);




const User = require("../models/User")(Sequelize, sequelize);
const UserGroup = require("../models/UserGroup")(Sequelize, sequelize);
const Establishment = require("../models/Establishment")(Sequelize, sequelize);
const Image = require("../models/Image")(Sequelize, sequelize);

const EnvCategory = require("../models/EnvCategory")(Sequelize, sequelize);

const EnvironmentItem = require("../models/EnvironmentItem")(
  Sequelize,
  sequelize
);

const Environment = require("../models/Environment")(
  Sequelize,
  sequelize,
  Establishment,
  EnvironmentItem
);

const InspectionItem = require("../models/InspectionItem")(
  Sequelize,
  sequelize,
  Image
);

const Inspection = require("../models/Inspection")(
  Sequelize,
  sequelize,
  InspectionItem
);

// sync wuth database
sequelize.sync();

// initialize data
init_db = async () => {
  var user = await User.findOne({ where: { login: "admin" } });
  if (user == null) {
    var admin = new User();

    admin.login = "admin";
    admin.password = bcrypt.hashSync("kiwkiwrocks", 1);

    admin.save();
  }
};

init_db();

module.exports = {
  Expediente,
  Medidor,
  Key,
  Service,
  Model,
  Surver,
  Customer,
  Property,
  Surveyor,
  Agency,
  User,
  UserGroup,
  Establishment,
  Image,
  EnvCategory,
  EnvironmentItem,
  Environment,
  InspectionItem,
  Inspection
};
