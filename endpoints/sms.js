const Joi = require("joi");
const jwt = require("jsonwebtoken");

const { sendSMS } = require("../auth/sms");
const { SMSStatus, User } = require("../persistence/db");

module.exports = [
  {
    method: "POST",
    path: "/sendverify",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);
        var code = Math.floor(Math.random() * (10000 - 1000) + 1000);

        var status = new SMSStatus();
        status.smsCode = code + "";
        status.phone = req.payload.phone;
        status.date = new Date();

        var message1 = sendSMS(req.payload.phone, code + "", message => {
          status.status = "sent";
          status.save();
        });

        return res.response({ status: "sent" }).code(201);
      },
      description: "Send SMS autenticate",
      tags: ["api", "sms"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          phone: Joi.string()
            .required()
            .description("telefone")
        }
      }
    }
  },
  {
    method: "POST",
    path: "/sentsmscode",
    config: {
      handler: async function(req, res) {
        var status = await SMSStatus.findOne({
          where: { phone: req.payload.phone }
        });

        if (status.smsCode == smsCode) {
          var user = await User.findOne({
            where: { phone: req.payload.phone }
          });
          user.status = "verified";
          user.save();

          return res.response({ status: "verified" }).code(201);
        } else {
          return res.response({ error: "invalid code" }).code(201);
        }
      },
      description: "Verify code",
      tags: ["api", "sms"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          phone: Joi.string()
            .required()
            .description("phone"),
          smsCode: Joi.string()
            .required()
            .description("SMS code")
        }
      }
    }
  }
];
