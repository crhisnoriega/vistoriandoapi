const fs = require("fs");

const Joi = require("joi");
const {
  Surveyor,
  Agency,
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/surveyor",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }

          var surveyor = await Surveyor.create(req.payload);

          surveyor.save();

          return res.response(surveyor);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register Surveyor",
      notes: "Surveyor",
      tags: ["api", "surveyor"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/surveyor/{surveyorid}",
    method: "PUT",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);
        console.log(req.params.agencyid);

        try {
          var surveyor = await Surveyor.update(req.payload, {
            where: { id: req.params.surveyorid }
          });

          return res.response(surveyor);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Update Surveyor",
      notes: "Surveyor",
      tags: ["api", "surveyor"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          surveyorid: Joi.string()
            .required()
            .description("agency id")
        },
        payload: {}
      }
    }
  },
  {
    path: "/surveyor/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var surveyors = await Surveyor.findAll({
            where: {
              name: {
                [Op.like]: "%" + req.query.query + "%"
              }
            },

            order: [["createdAt", "DESC"]]
          });

          return res.response(surveyors);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query surveyor info",
      notes: "Query for surveyor info",
      tags: ["api", "surveyor"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .required()
            .description("query")
        }
      }
    }
  },
  {
    path: "/surveyor/{surveyorid}",
    method: "DELETE",
    config: {
      handler: async function(req, res) {
        
        console.log(req.params.surveyorid);

        try {
          var result = await Surveyor.destroy({
            where: { id: req.params.surveyorid }
          });

          return res.response(result);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Update Surveyor",
      notes: "Surveyor",
      tags: ["api", "surveyor"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          surveyorid: Joi.string()
            .required()
            .description("agency id")
        }
      }
    }
  }
];
