module.exports = function(Sequelize, sequelize) {
  class UserGroup extends Sequelize.Model {}
  UserGroup.init({
    id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    name: Sequelize.STRING,
    priority: {
      type: Sequelize.ENUM,
      values: ["normal","superuser",""]
    },
  }, { sequelize, modelName: "usergroup" });



  return UserGroup;
};
