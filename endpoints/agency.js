const fs = require("fs");

const Joi = require("joi");
const {
  Agency,
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/agency",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }

          var agency = await Agency.create(req.payload);

          agency.save();

          return res.response(agency);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register Agency",
      notes: "Agency",
      tags: ["api", "agency"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          name: Joi.string().description("establishment id"),
          cnpj: Joi.string().description("CNPJ"),
          socialname: Joi.string().description("Social Name"),
          description: Joi.string(),
          photo: Joi.string().description("photo base64"),
          type: Joi.string(),
          cep: Joi.string(),
          street: Joi.string(),
          number: Joi.string(),
          complemento: Joi.string(),
          bairro: Joi.string(),
          city: Joi.string(),
          state: Joi.string(),
          fone1: Joi.string(),
          fone2: Joi.string(),
          email: Joi.string()
        }
      }
    }
  },
  {
    path: "/agency/{agencyid}",
    method: "PUT",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);
        console.log(req.params.agencyid);

        try {
          var agency = await Agency.update(req.payload, {
            where: { id: req.params.agencyid }
          });

          return res.response(agency);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Update Agency",
      notes: "Agency",
      tags: ["api", "agency"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          agencyid: Joi.string()
            .required()
            .description("agency id")
        },
        payload: {}
      }
    }
  },
  {
    path: "/agency/{agencyid}",
    method: "DELETE",
    config: {
      handler: async function(req, res) {
        console.log(req.params.agencyid);

        try {
          var result = await Agency.destroy({
            where: { id: req.params.agencyid }
          });

          return res.response(result);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Delete Agency",
      notes: "Agency",
      tags: ["api", "agency"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          agencyid: Joi.string()
            .required()
            .description("agency id")
        }
      }
    }
  },
  {
    method: "POST",
    path: "/product/uploadimage/{productid}",
    config: {
      payload: {
        output: "stream",
        allow: "multipart/form-data" // important
      },
      handler: async function(req, res) {
        var file = req.payload.file;

        console.log("upload image");
        console.log(file.filename);

        var product = await Product.findOne({
          where: { id: req.params.productid }
        });

        const filename = file.hapi.filename;
        const data = file._data;

        fs.writeFileSync(filename, data);
        uploadS3FileSync(filename, filename, product);
        return res.response({ status: "OK" }).code(200);
      },
      description: "Add image to product",
      notes: "Product",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          productid: Joi.string()
            .required()
            .description("base64 endode")
        },
        payload: {}
      }
    }
  },
  {
    method: "PUT",
    path: "/product/image/{productid}",
    config: {
      handler: async function(req, res) {
        var product = await Product.findOne({
          where: { id: req.params.productid }
        });

        if (product != null) {
          var base64Image = req.payload.base64.split(";base64,").pop();
          fs.writeFileSync(req.payload.filename, base64Image, {
            encoding: "base64"
          });
          uploadS3FileSync(req.payload.filename, req.payload.filename, product);
          return res.response({ status: "OK" }).code(200);
        } else {
          return res.response({ status: "product not found" }).code(501);
        }
      },
      description: "Add image to product",
      notes: "Product",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          productid: Joi.string()
            .required()
            .description("base64 endode")
        },
        payload: {
          base64: Joi.string()
            .required()
            .description("base64 endode"),
          filename: Joi.string()
            .required()
            .description("file name")
        }
      }
    }
  },
  {
    path: "/product/{productid}",
    method: "GET",
    config: {
      handler: async function(req, res) {
        var products = await Product.findAll({
          where: { id: req.params.productid }
        });

        var result = [];

        for (p in products) {
          console.log(products[p].id);
          var images = await Image.findAll({
            where: { productId: req.params.productid }
          });
          result.push({ product: products[p], images: images });
        }

        return res.response(result);
      },
      description: "Retrieve product info",
      notes: "product info",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          productid: Joi.string()
            .required()
            .description("product id")
        }
      }
    }
  },
  {
    path: "/agency/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var agencies = await Agency.findAll({
            where: {
              name: {
                [Op.like]: "%" + req.query.query + "%"
              }
            },

            order: [["createdAt", "DESC"]]
          });

          return res.response(agencies);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query agency info",
      notes: "Query for agency info",
      tags: ["api", "agency"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .required()
            .description("query")
        }
      }
    }
  },
  {
    path: "/product/category",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var establish = await Establishment.findOne({
            where: { id: req.payload.establishmentid }
          });

          console.log("establish:" + JSON.stringify(establish));

          var category = await Category.create(req.payload.category);

          category.setEstablishment(establish);
          category.save();

          var cats = await Category.findAll({
            where: { establishmentId: req.payload.establishmentid }
          });

          return res.response(cats);
        } catch (e) {
          console.log(e);
          return res.response(e);
        }
      },
      description: "Register Category",
      notes: "Register Category",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          establishmentid: Joi.string().description("establishment"),
          category: {
            id: Joi.string().description("id"),
            name: Joi.string().description("name")
          }
        }
      }
    }
  },
  {
    path: "/product/categories",
    method: "GET",
    config: {
      handler: async function(req, res) {
        console.log(req.query);

        try {
          var cats = await Category.findAll({
            where: { establishmentId: req.query.establishmentid }
          });

          return res.response(cats);
        } catch (e) {
          console.log(e);
          return res.response(e);
        }
      },
      description: "Register Category",
      notes: "Register Category",
      tags: ["api", "product"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          establishmentid: Joi.string().description("establishment")
        }
      }
    }
  }
];
