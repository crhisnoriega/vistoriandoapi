const fs = require("fs");

const Joi = require("joi");
const { Establishment } = require("../persistence/db");

module.exports = [
  {
    path: "/establishment",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          var establish = await Establishment.create(req.payload);
          return res.response(establish);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register establishment",
      notes: "Register establishment",
      tags: ["api", "establishment"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {
          id: Joi.string()
            .required()
            .description("establishment  id"),
          name: Joi.string()
            .required()
            .description("establishment  name"),
          shortName: Joi.string()
          .description("establishment short name")
        }
      }
    }
  }
];
