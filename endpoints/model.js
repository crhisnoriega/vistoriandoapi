const fs = require("fs");

const Joi = require("joi");
const {
  Model,
  Surver,
  Customer,
  Property,
  Agency,
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/model",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }

          var model = await Model.create(req.payload);

          model.save();

          return res.response(model);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register model",
      notes: "model",
      tags: ["api", "model"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/model/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var models = await Model.findAll({
            order: [["createdAt", "DESC"]]
          });

          return res.response(models);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query models info",
      notes: "Query for models info",
      tags: ["api", "model"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string().description("query")
        }
      }
    }
  },
  {
    path: "/model/{modelid}",
    method: "DELETE",
    config: {
      handler: async function(req, res) {
        console.log(req.params.modelid);

        try {
          var result = await Model.destroy({
            where: { id: req.params.modelid }
          });

          return res.response(result);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Update Model",
      notes: "Model",
      tags: ["api", "model"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          modelid: Joi.string()
            .required()
            .description("model id")
        }
      }
    }
  },
  {
    path: "/model/{modelid}",
    method: "PUT",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);
        console.log(req.params.modelid);

        try {
          var model = await Model.update(req.payload, {
            where: { id: req.params.modelid }
          });

          return res.response(model);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Update Model",
      notes: "Model",
      tags: ["api", "model"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          modelid: Joi.string()
            .required()
            .description("model id")
        },
        payload: {}
      }
    }
  }
];
