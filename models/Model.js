module.exports = function(Sequelize, sequelize) {
  class Model extends Sequelize.Model {}
  Model.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: Sequelize.STRING,
      status: {
        type: Sequelize.ENUM,
        values: [
          "enable",
          "disable"
        ]
      },

      environments: {
        type: Sequelize.TEXT("long"),
        get: function() {
          try {
            var json = JSON.parse(this.getDataValue("environments"));
            console.log(json);
            return json;
          } catch (e) {}
        },
        set: function(val) {
          try {
            return this.setDataValue("environments", JSON.stringify(val));
          } catch (e) {}
        }
      },

      itens: {
        type: Sequelize.TEXT("long"),
        get: function() {
          try {
            var json = JSON.parse(this.getDataValue("itens"));
            console.log(json);
            return json;
          } catch (e) {}
        },
        set: function(val) {
          try {
            return this.setDataValue("itens", JSON.stringify(val));
          } catch (e) {}
        }
      }
    },
    { sequelize, modelName: "model" }
  );

  return Model;
};
