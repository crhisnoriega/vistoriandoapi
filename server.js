// server libs
const Hapi = require("hapi");
const Inert = require("inert");
const Vision = require("vision");
const Joi = require("joi");
const HapiSwagger = require("hapi-swagger");

// encrypts
const bcrypt = require("bcrypt");
const validateToken = require("./auth/myjwt");

// endpoints
const user = require("./endpoints/user");
const environment = require("./endpoints/environment");
const establishment = require("./endpoints/establishment");
const agency = require("./endpoints/agency");
const surveyor = require("./endpoints/surveyor");
const property = require("./endpoints/property");
const customer = require("./endpoints/customer");
const surver = require("./endpoints/surver");
const model = require("./endpoints/model");

const service = require("./endpoints/service");

const key = require("./endpoints/key");
const medidor = require("./endpoints/medidor");
const expediente = require("./endpoints/expediente");

// api
const server = Hapi.Server({
  host: "0.0.0.0",
  port: 3002
});

const options = {
  basePath: "/v1",
  info: {
    title: "Vistoriando API documentation",
    description: "REST API for Vistoriando app",
    version: "0.0.1"
  },
  grouping: "tags",
  tags: [
    {
      name: "user",
      description: "User operations"
    },
    {
      name: "environments",
      description: "Environment operations"
    },
    {
      name: "agency",
      description: "Agency operations"
    },
    {
      name: "surveyor",
      description: "Surveyor operations"
    },
    {
      name: "property",
      description: "Property operations"
    },
    {
      name: "customer",
      description: "Customer operations"
    },
    {
      name: "surver",
      description: "Surver operations"
    },
    {
      name: "model",
      description: "Model operations"
    },
    {
      name: "service",
      description: "Service operations"
    },
    {
      name: "key",
      description: "Key operations"
    },
    {
      name: "medidor",
      description: "Medidor operations"
    },
    {
      name: "expediente",
      description: "Expediente operations"
    }
  ]
};

var routes = [].concat(
  expediente,
  medidor,
  key,
  service,
  model,
  user,
  environment,
  establishment,
  agency,
  surveyor,
  property,
  customer,
  surver
);

const startServer = async () => {
  // setting plugins
  await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: options
    }
  ]);

  // setting jwt - json web token
  await server.register(require("hapi-auth-jwt2"));

  server.auth.strategy("jwt", "jwt", {
    key: "vistoriando rocks",
    validate: validateToken,
    verifyOptions: { algorithms: ["HS256"] }
  });

  // setting routes
  server.realm.modifiers.route.prefix = "/v1";
  await server.route(routes);
  
  await server.register(require('@hapi/inert'));

  // start server
  await server.start();

  console.log(`Server is running at ${server.info.uri}`);

  process.on("unhandledRejection", err => {
    console.log(err);

    // for debug only
    process.exit(1);
  });
};

startServer();
