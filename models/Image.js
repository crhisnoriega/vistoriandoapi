module.exports = function(Sequelize, sequelize, Product) {
  class Image extends Sequelize.Model {}
  Image.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      productId: Sequelize.STRING,
      url: Sequelize.STRING,
      base64: Sequelize.STRING
    },
    { sequelize, modelName: "image" }
  );

  return Image;
};
