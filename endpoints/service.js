const fs = require("fs");

const Joi = require("joi");
const {
  Service,
  Model,
  Surver,
  Customer,
  Property,
  Agency,
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/service",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }

          var service = await Service.create(req.payload);

          service.save();

          return res.response(service);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register service",
      notes: "service",
      tags: ["api", "service"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/service/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var services = await Service.findAll({
            order: [["createdAt", "DESC"]]
          });

          return res.response(services);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query services info",
      notes: "Query for services info",
      tags: ["api", "service"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string()
            .description("query")
        }
      }
    }
  }
];
