module.exports = function(Sequelize, sequelize) {
  class EnvCategory extends Sequelize.Model {}
  EnvCategory.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: { type: Sequelize.STRING, comment: "Category name" },
      shortname: { type: Sequelize.STRING, comment: "Short category name" },
      description: Sequelize.STRING
    },
    { sequelize, modelName: "envcategory" }
  );


  return EnvCategory;
};
