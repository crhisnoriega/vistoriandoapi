module.exports = function(Sequelize, sequelize) {
  class Expediente extends Sequelize.Model {}
  Expediente.init(
    {
      id: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      name: Sequelize.STRING,
      status: {
        type: Sequelize.ENUM,
        values: ["enable", "disable"]
      },
      rules: {
        type: Sequelize.TEXT("long"),
        get: function() {
          try {
            var json = JSON.parse(this.getDataValue("rules"));
            console.log(json);
            return json;
          } catch (e) {}
        },
        set: function(val) {
          try {
            return this.setDataValue("rules", JSON.stringify(val));
          } catch (e) {}
        }
      }
    },
    { sequelize, modelName: "expediente" }
  );

  return Expediente;
};
