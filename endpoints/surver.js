const fs = require("fs");

const Joi = require("joi");
const {
  Surver,
  Customer,
  Property,
  Agency,
  Category,
  Product,
  Establishment,
  Image
} = require("../persistence/db");
const uploadS3FileSync = require("../aws/s3");

const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const sequelize = new Sequelize(
  "mysql://vistoriando:3&9Zy2pn@db.vistoriando.com/vistoriando"
);

const uuidv1 = require("uuid/v1");

module.exports = [
  {
    path: "/surver",
    method: "POST",
    config: {
      handler: async function(req, res) {
        console.log(req.payload);

        try {
          if (req.payload.id == null) {
            req.payload.id = uuidv1();
          }

          var surver = await Surver.create(req.payload);

          surver.save();

          return res.response(surver);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Register surver",
      notes: "surver",
      tags: ["api", "surver"],
      validate: {
        options: {
          allowUnknown: true
        },
        payload: {}
      }
    }
  },
  {
    path: "/surver/search",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var survers = await Surver.findAll({
            order: [["createdAt", "DESC"]]
          });

          return res.response(survers);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query survers info",
      notes: "Query for survers info",
      tags: ["api", "surver"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string().description("query")
        }
      }
    }
  },
  {
    path: "/surver/group",
    method: "GET",
    config: {
      handler: async function(req, res) {
        try {
          var results = await sequelize.query(
            "select MONTH(createdAt) as month, YEAR(createdAt) as year, count(*) as qtde from survers GROUP BY YEAR(createdAt), MONTH(createdAt)"
          );

          return res.response(results[0]);
        } catch (e) {
          console.log(e);
          return res.response({ error: JSON.stringify(e) });
        }
      },
      description: "Query survers info",
      notes: "Query for survers info",
      tags: ["api", "surver"],
      validate: {
        options: {
          allowUnknown: true
        },
        query: {
          query: Joi.string().description("query")
        }
      }
    }
  },
  {
    path: "/surver/{surverid}",
    method: "DELETE",
    config: {
      handler: async function(req, res) {
        console.log(req.params.surveyorid);

        try {
          var result = await Surver.destroy({
            where: { id: req.params.surverid }
          });

          return res.response(result);
        } catch (e) {
          console.log(e);
          return res.response(JSON.stringify(e)).code(501);
        }
      },
      description: "Update Surver",
      notes: "Surver",
      tags: ["api", "surver"],
      validate: {
        options: {
          allowUnknown: true
        },
        params: {
          surverid: Joi.string()
            .required()
            .description("surver id")
        }
      }
    }
  }
];
