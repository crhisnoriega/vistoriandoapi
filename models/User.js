module.exports = function(Sequelize, sequelize) {
  class User extends Sequelize.Model {}
  User.init(
    {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      login: Sequelize.STRING,
      phone: Sequelize.STRING,
      firstName: Sequelize.STRING,
      lastName: Sequelize.STRING,
      password: Sequelize.STRING,
      token: { type: Sequelize.STRING, comment: "Session token" },
      tokenDate: { type: Sequelize.STRING, comment: "Session date token" },
      email: Sequelize.STRING,
      fcmToken: { type: Sequelize.STRING, comment: "FCM token" },
      status: Sequelize.ENUM("new", "verified", "pendent", "disable", "enable"),
      profileType: {
        type: Sequelize.ENUM,
        values: ["manager", "inspector", "admin"]
      },
      photo: Sequelize.TEXT("long"),
      groupId: Sequelize.STRING
    },
    { sequelize, modelName: "user" }
  );

  return User;
};
